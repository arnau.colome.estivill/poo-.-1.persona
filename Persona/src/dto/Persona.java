package dto;

public class Persona {
	
	final String sex="H";
	
	private String nombre;
	private int edad;
	private String DNI;
	private String sexo;
	private double peso;
	private double altura;
	
	/*
	Un constructor por defecto.
	Un constructor con el nombre, edad y sexo, el resto por defecto.
	Un constructor con todos los atributos como parámetro.
	*/
	
	public Persona() {
		this.nombre="";
		this.edad=0;
		this.DNI="";
		this.sexo=sex;
		this.peso=0;
		this.altura=0;
	}
	
	public Persona(String nombre, int edad, String sexo) {
		this.nombre = nombre;
		this.edad = edad;
		this.sexo = sexo;
	}

	public Persona(String nombre, int edad, String DNI, String sexo, double peso, double altura) {
		this.nombre = nombre;
		this.edad = edad;
		this.DNI = DNI;
		this.sexo = sexo;
		this.peso = peso;
		this.altura = altura;
	}
	
	
	@Override
	public String toString() {
		return "Persona [nombre=" + nombre + ", edad=" + edad + ", DNI=" + DNI + ", sexo=" + sexo + ", peso=" + peso
				+ ", altura=" + altura + "]";
	}
	
		
	//getters

	private String getNombre() {
		return nombre;
	}

	private int getEdad() {
		return edad;
	}

	private String getDNI() {
		return DNI;
	}

	private String getSexo() {
		return sexo;
	}

	private double getPeso() {
		return peso;
	}

	private double getAltura() {
		return altura;
	}

	
	//setters
	
	private void setNombre(String nombre) {
		this.nombre = nombre;
	}

	private void setEdad(int edad) {
		this.edad = edad;
	}

	private void setDNI(String dNI) {
		DNI = dNI;
	}

	private void setSexo(String sexo) {
		this.sexo = sexo;
	}

	private void setPeso(double peso) {
		this.peso = peso;
	}

	private void setAltura(double altura) {
		this.altura = altura;
	}
	
	
}
