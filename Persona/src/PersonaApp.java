import dto.Persona;

public class PersonaApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Persona e1 = new Persona();
		Persona e2 = new Persona("Arnau", 20, "38875305E", "H", 80, 190);
		Persona e3 = new Persona("Jose", 80, "H");
		
		System.out.println("e1: " + e1);
		System.out.println("e2: " + e2);
		System.out.println("e3: " + e3);
		
	}

}
